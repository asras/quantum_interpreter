# Quantum Interpreter

This is my rewrite of https://www.stylewarning.com/posts/quantum-interpreter/ in Python.


## Quickstart

Clone the repo and `cd` into it.

```
python3 -m pip install -e .
```

Then run
```
python3 -m qint 
```

to start the repl and
```
python3 -m pytest tests/
```
to run the tests.


## Examples
Example programs can be found in tests/test_program.py.
