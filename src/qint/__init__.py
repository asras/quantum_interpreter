import numpy as np

I = np.array([[1, 0], [0, 1]])
X = np.array([[0, 1], [1, 0]])
SWAP = np.array([[1, 0, 0, 0],
                 [0, 0, 1, 0],
                 [0, 1, 0, 0],
                 [0, 0, 0, 1]])
H = np.array([[1, 1], [1, -1]]) / np.sqrt(2)
# Second bit is control bit
CNOT = np.array([[1, 0, 0, 0],
                 [0, 1, 0, 0],
                 [0, 0, 0, 1],
                 [0, 0, 1, 0]])
