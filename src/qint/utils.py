import numpy as np
from qint import I, SWAP
from functools import reduce
from typing import List
from qint.permutation_utils import permutation_to_transpositions, transpositions_to_adjacent_transpositions


def parse_bit_string(bit_string: str) -> int:
    """Convert bit string into integer"""
    num = 0
    for ch in bit_string:
        num *= 2
        if ch == '0':
            num += 0
        elif ch == '1':
            num += 1
        else:
            raise ValueError(f"Invalid bit string: {bit_string}")
    return num


def int_to_bit_string(num: int, nbits: int) -> str:
    """Convert integer into bit string"""
    if num == 0:
        return '0' * nbits

    bit_string = ''
    while num > 0:
        bit_string = str(num & 1) + bit_string
        num //= 2

    return bit_string.rjust(nbits, '0')


def sample_state(state: np.ndarray) -> int:
    """Sample from the quantum state"""
    norm = (state * state.conjugate()).sum()
    probabilities = (state * state.conjugate() / norm).real
    indices = np.arange(0, state.shape[0])
    return int(np.random.choice(indices, p=probabilities))


def kronecker_exp(mat, n):
    """Calculate the kronecker product of a matrix with itself n times."""
    if n == 0:
        return np.identity(1)

    result = mat
    for i in range(n - 1):
        result = np.kron(result, mat)

    return result


def dimension_qubits(mat):
    """Calculate how many qubits a matrix acts on."""
    return int(np.log(mat.shape[0]) / np.log(2))


def lift(mat: np.ndarray, index: int, nqubits: int) -> np.ndarray:
    """Lift an n-adjacent-qubit operator to an "nqubits"-qubit operator.

    The operator is inserted at index "index".
    """
    gate_size = dimension_qubits(mat)
    assert gate_size <= nqubits
    left = kronecker_exp(I, nqubits - index - gate_size)
    right = kronecker_exp(I, index)

    result = np.kron(left, np.kron(mat, right))
    assert result.shape == (2 ** nqubits, 2 ** nqubits), f"result.shape = {result.shape}, expected = {2**nqubits}"
    return result


def swap(i: int, n_bits: int) -> np.ndarray:
    """Return an operator that swaps the ith and (i+1)th qubits."""
    return lift(SWAP, i, n_bits)


def transpositions_to_operator(trans: List[int], n_bits):
    """Return an operator that performs the given transpositions."""
    if len(trans) == 0:
        return lift(I, 0, n_bits)

    trans = list(trans)
    result = swap(trans[0], n_bits)
    for i in range(1, len(trans)):
        # result = result @ swap(trans[i], n_bits)
        result = swap(trans[i], n_bits) @ result

    return result
    return reduce(lambda acc, t: acc @ swap(t, n_bits),
                  trans,
                  lift(I, 0, n_bits))


def bit_string_idx(operator: np.ndarray, idx1: str, idx2: str) -> np.complex128:
    """Index an operator using two bit strings"""
    index1 = parse_bit_string(idx1)
    index2 = parse_bit_string(idx2)

    return operator[index1, index2]


def permutation_to_operator(perm: str, nbits: int) -> np.ndarray:
    """Turn a permutation to an operator.

    E.g. the permutation "201" will place bit 2 in the 0th position,
    bit 0 in the 1st position, and bit 1 in the 2nd position.
    """
    trans = permutation_to_transpositions(perm)
    trans = transpositions_to_adjacent_transpositions(trans)

    op = transpositions_to_operator(trans, nbits)

    return op
