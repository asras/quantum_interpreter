from typing import List, Tuple

Transposition = Tuple[int, int]


def permutation_to_transpositions(permutation: str) -> List[Transposition]:
    """Transform a permutation into a list of transpositions.

    The permutation is given as a string of digits, e.g. "102345" for the
    permutation (1 0 2 3 4 5).
    """
    permutation = [int(x) for x in permutation]
    swaps = []
    for src in range(len(permutation)):
        dest = permutation[src]
        while dest < src:
            dest = permutation[dest]
        if src < dest:
            swaps.append((src, dest))
        elif src > dest:
            raise Exception("Unreachable")
            swaps.append((dest, src))

    return swaps


def transposition_to_adjacent_transpositions(tranpos: Transposition) -> List[int]:
    """Transform a transposition into a list of adjacent transpositions.

    For example, (0, 2) is transformed into [(0, 1), (1, 2), (0, 1)]
                 which is represented as [0, 1, 0]
                 (0, 3) is transformed into [(0, 1), (1, 2), (2, 3), (1, 2), (0, 1)]
                 which is represented as [0, 1, 2, 1, 0]
    """
    src, dest = tranpos
    if src > dest:
        src, dest = dest, src

    result = []
    for i in range(src, dest):
        result.append(i)
    for i in range(dest - 2, src - 1, -1):
        result.append(i)
    return result


def transpositions_to_adjacent_transpositions(transpos: List[Transposition]) -> List[int]:
    """Transform a list of transpositions into a list of adjacent transpositions.

    For example, [(0, 2), (1, 3)] is transformed into
                 [0, 1, 0, 1, 2, 1].
                 [(0, 2), (0, 1)] is transformed into
                 [0, 1].
    """
    temp = []
    for tranpos in transpos:
        temp.extend(transposition_to_adjacent_transpositions(tranpos))

    result = []
    # Eliminate any adjacent, identical transpositions
    i = 0
    while i < len(temp):
        if i < len(temp) - 1 and temp[i] == temp[i + 1]:
            i += 2
        else:
            result.append(temp[i])
            i += 1

    return result


def invert_perm(perm: str) -> str:
    """Invert a permutation."""
    perm = [int(x) for x in perm]
    result = [0] * len(perm)
    for i, x in enumerate(perm):
        result[x] = i
    return "".join(str(x) for x in result)
