import numpy as np
from typing import List
from qint import SWAP, H
from qint.main import Instruction


def cphase(angle: float):
    return np.array([[1, 0, 0, 0],
                     [0, 1, 0, 0],
                     [0, 0, 1, 0],
                     [0, 0, 0, np.exp(1j * angle)]])


def bit_reversal(qubits: List[int]) -> List[Instruction]:
    if len(qubits) < 2:
        return []

    result = []
    for i in range(len(qubits) // 2):
        qub = qubits[i]
        qub_2 = qubits[len(qubits) - i - 1]
        result.append(Instruction("GATE", SWAP, [qub, qub_2]))

    return result


def _qft(qubits: List[int]) -> List[Instruction]:
    if len(qubits) == 1:
        return [Instruction("GATE", H, [qubits[0]])]

    result = []
    n = len(qubits)
    for i in range(1, len(qubits)):
        angle = np.pi / (2 ** (n - i))
        result.append(Instruction("GATE", cphase(angle), [qubits[0], qubits[i]]))

    result = qft(qubits[1:]) + result
    result.append(Instruction("GATE", H, [qubits[0]]))

    return result


def qft(qubits: List[int]):
    result = _qft(qubits)
    result.extend(bit_reversal(qubits))

    return result
