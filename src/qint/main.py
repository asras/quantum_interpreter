import numpy as np
from dataclasses import dataclass
from typing import List
from qint.permutation_utils import invert_perm
from qint.utils import parse_bit_string, sample_state, lift, int_to_bit_string, permutation_to_operator


@dataclass
class Instruction:
    type: str
    gate: np.ndarray
    qubits: List[int]

    def from_string(source: str):
        """Convert a string of format "GATE gate qubits+" or "MEASURE" to an Instruction."""
        tokens = source.split(';')
        if tokens[0] == 'GATE':
            # A little bit of trickery to make the parsing of the gate easy
            gate = np.array(eval(tokens[1]))
            qubits = [int(q) for q in tokens[2].strip().split(" ") if q != ""]
            assert len(qubits) > 0, "No qubits provided"
            assert gate.shape[0] == len(qubits) * 2, f"Gate {gate} has {gate.shape[0] // 2} qubits, but {len(qubits)} were provided"
            return Instruction('GATE', gate, qubits)
        elif tokens[0] == 'MEASURE':
            assert len(tokens) == 1, "MEASURE instruction does not take any arguments"
            return Instruction('MEASURE', None, None)
        else:
            raise ValueError(f"Invalid instruction type: {tokens[0]}")


@dataclass
class Machine:
    quantum_state: np.ndarray
    measurement_register: int

    def init(n: int):
        qstate = np.zeros(2**n)
        # Convention: Starting state is |000...0>
        qstate[0] = 1
        return Machine(qstate, 0)

    def _run(self, program: List[Instruction]):
        for instr in program:
            if instr.type == 'GATE':
                self.apply_gate(instr.gate, instr.qubits)
            elif instr.type == 'MEASURE':
                self.observe()
            else:
                raise ValueError(f"Invalid instruction type: {instr.type}")

    def run(self, program: List[Instruction]) -> str:
        self._run(program)

        return int_to_bit_string(self.measurement_register, self.n_bits())

    # # Utility functions
    def n_bits(self) -> int:
        return int(np.log(self.quantum_state.shape[0]) / np.log(2))

    def get_amplitude(self, bit_string: str) -> complex:
        if len(bit_string) > self.n_bits():
            raise ValueError(f"Too many bits: {len(bit_string)} > {self.n_bits()}")

        index = parse_bit_string(bit_string)
        return self.quantum_state[index]

    def get_measurement_bit(self, n: int) -> int:
        return (self.measurement_register >> n) & 1

    def collapse(self, index: int):
        self.quantum_state *= 0.0
        self.quantum_state[index] = 1.0
    # # END Utility functions

    # # Evolution functions
    def observe(self):
        outcome_idx = sample_state(self.quantum_state)
        self.collapse(outcome_idx)
        self.measurement_register = outcome_idx

    def apply_gate(self, gate: np.ndarray, qubits: List[int]):
        assert len(qubits) * 2 == gate.shape[0]
        if len(qubits) == 1:
            self.apply_1qb_gate(gate, qubits[0])
        else:
            self.apply_nqb_gate(gate, qubits)

    def apply_1qb_gate(self, gate: np.ndarray, qubit: int):
        U = lift(gate, qubit, self.n_bits())
        self.quantum_state = U @ self.quantum_state

    def apply_nqb_gate(self, gate: np.ndarray, qubits: List[int]):
        assert len(qubits) * 2 == gate.shape[0]
        # Matrix for the gate acting on the first qubits
        U_01 = lift(gate, 0, self.n_bits())

        # Permutation that puts the qubits in the positions
        # that the gate expects
        perm = [i for i in qubits] \
            + [i for i in range(self.n_bits()) if i not in qubits]

        pi = permutation_to_operator(perm, self.n_bits())
        piinv = permutation_to_operator(invert_perm(perm), self.n_bits())
        Upq = piinv @ U_01 @ pi
        self.quantum_state = Upq @ self.quantum_state
        return
    # # END Evolution functions


def apply_operator(matrix: np.ndarray, column: np.ndarray) -> np.ndarray:
    return matrix @ column


def compose_operators(matrix1: np.ndarray, matrix2: np.ndarray) -> np.ndarray:
    return matrix1 @ matrix2
