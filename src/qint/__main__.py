from qint import I, X, SWAP
from qint.main import Machine, Instruction
import re


def print_help():
    print("Create a machine via '<name> = MACHINE <qubits>'")
    print("Create a gate instruction via '<name> = GATE; <matrix>; <qubits>'")
    print("Create a measurement instruction via '<name> = MEASURE'")
    print("Create a program via '<name> = [<instruction>, ...]'")
    print("Run a program via '<name>.run(<program>)'")
    print("Get a measurement bit via '<name>.get_measurement_bit(<qubit>)'")
    print("Built-in operators are I, X, SWAP")


def array_2_str(arr):
    s = "["
    for i, row in enumerate(arr):
        if i > 0:
            s += ", "
        s += "[" + ",".join(str(v) for v in row) + "]"

    return s + "]"


def repl():
    print("QINT REPL")
    print("Type 'exit' to exit")
    print("Type 'help' for help")
    print_help()

    exp = r"(?P<name>\w+)\s+=\s+MACHINE\s+(?P<qubits>\d+)"
    gate_exp = r"(?P<name>\w+)\s+=\s+GATE\s*;\s*(?P<matrix>.+)\s*;\s*(?P<qubits>.+)"
    measure_exp = r"(?P<name>\w+)\s+=\s+MEASURE"
    prog_exp = r"(?P<name>\w+)\s+=\s+(?P<program>\[.+\])"

    while True:
        try:
            inp = input(">>> ")
            if inp == "exit" or len(inp) == 0:
                break
            inp = inp.replace("X", array_2_str(X))
            inp = inp.replace("I", array_2_str(I))
            inp = inp.replace("SWAP", array_2_str(SWAP))
            print(inp)
            if inp.strip().lower() == "help":
                print_help()
                continue

            m = re.match(exp, inp)
            if m:
                name = m.group("name")
                qubits = int(m.group("qubits"))
                globals()[name] = Machine.init(qubits)
                continue

            gate_m = re.match(gate_exp, inp)
            if gate_m:
                name = gate_m.group("name")
                matrix = eval(gate_m.group("matrix"))
                qubits = int(gate_m.group("qubits"))
                globals()[name] = Instruction.from_string(f"GATE; {matrix}; {qubits}")
                continue

            measure_m = re.match(measure_exp, inp)
            if measure_m:
                name = measure_m.group("name")
                globals()[name] = Instruction("MEASURE", None, None)
                continue

            prog_m = re.match(prog_exp, inp)
            if prog_m:
                name = prog_m.group("name")
                program = eval(prog_m.group("program"))
                globals()[name] = program
                continue

            print(eval(inp, globals()))
        except EOFError:
            print()
            break
        except Exception as e:
            print(f"Exception: {e}")


repl()
