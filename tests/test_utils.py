import numpy as np
from qint.utils import int_to_bit_string, parse_bit_string, permutation_to_operator


def test_to_int_and_back():
    src = "1101011"
    assert int_to_bit_string(parse_bit_string(src), len(src)) == src


def test_to_int_and_back_2():
    src = "0001011"
    assert int_to_bit_string(parse_bit_string(src), len(src)) == src


def test_0_int():
    val = 0
    assert int_to_bit_string(val, 5) == "00000"


def test_parse_bit_string():
    src = "0011"
    assert parse_bit_string(src) == 3

    src = "0101"
    assert parse_bit_string(src) == 5

    src = "0111"
    assert parse_bit_string(src) == 7

    src = "1010"
    assert parse_bit_string(src) == 10

    src = "0110"
    assert parse_bit_string(src) == 6

    src = "1011"
    assert parse_bit_string(src) == 11


def test_transpos_to_op():
    from qint.utils import transpositions_to_operator
    from qint.permutation_utils import transpositions_to_adjacent_transpositions, permutation_to_transpositions
    # Swap 0 and 3
    perm = "3120"
    trans = transpositions_to_adjacent_transpositions(permutation_to_transpositions(perm))
    assert trans == [0, 1, 2, 1, 0]
    op = transpositions_to_operator(trans, 4)

    state = np.zeros(2**4)
    state[0] = 1
    assert np.allclose(op @ state, state)
    state[0] = 0
    idx = parse_bit_string("1000")
    state[idx] = 1
    expected = np.zeros(2**4)
    expected_idx = parse_bit_string("0001")
    expected[expected_idx] = 1
    actual = op @ state
    assert np.allclose(actual, expected)


def test_perm_to_op():
    from qint.utils import transpositions_to_operator
    from qint.permutation_utils import transpositions_to_adjacent_transpositions, permutation_to_transpositions
    # Do complex permutation
    perm = "3201"
    trans = transpositions_to_adjacent_transpositions(permutation_to_transpositions(perm))
    op = transpositions_to_operator(trans, 4)

    state = np.zeros(2**4)
    expected = np.zeros(2**4)
    state[0] = 1
    assert np.allclose(op @ state, state)
    state *= 0
    expected *= 0
    idx = parse_bit_string("1000")
    state[idx] = 1
    expected_idx = parse_bit_string("0001")
    expected[expected_idx] = 1
    actual = op @ state
    assert np.allclose(actual, expected)

    state *= 0
    expected *= 0
    idx = parse_bit_string("0100")
    state[idx] = 1
    expected_idx = parse_bit_string("0010")
    expected[expected_idx] = 1
    actual = op @ state
    assert np.allclose(actual, expected)

    state *= 0
    expected *= 0
    idx = parse_bit_string("0010")
    state[idx] = 1
    expected_idx = parse_bit_string("1000")
    expected[expected_idx] = 1
    actual = op @ state
    assert np.allclose(actual, expected)

    state *= 0
    expected *= 0
    idx = parse_bit_string("0001")
    state[idx] = 1
    expected_idx = parse_bit_string("0100")
    expected[expected_idx] = 1
    actual = op @ state
    assert np.allclose(actual, expected)


def test_perm_to_op2():
    # Do complex permutation
    perm = "1320"
    op = permutation_to_operator(perm, 4)

    state = np.zeros(2**4)
    expected = np.zeros(2**4)
    state[0] = 1
    assert np.allclose(op @ state, state)
    state *= 0
    expected *= 0
    idx = parse_bit_string("1000")
    state[idx] = 1
    expected_idx = parse_bit_string("0010")
    expected[expected_idx] = 1
    actual = op @ state
    assert np.allclose(actual, expected)

    state *= 0
    expected *= 0
    idx = parse_bit_string("0100")
    state[idx] = 1
    expected_idx = parse_bit_string("0100")
    expected[expected_idx] = 1
    actual = op @ state
    assert np.allclose(actual, expected)

    state *= 0
    expected *= 0
    idx = parse_bit_string("0010")
    state[idx] = 1
    expected_idx = parse_bit_string("0001")
    expected[expected_idx] = 1
    actual = op @ state
    assert np.allclose(actual, expected)

    state *= 0
    expected *= 0
    idx = parse_bit_string("0001")
    state[idx] = 1
    expected_idx = parse_bit_string("1000")
    expected[expected_idx] = 1
    actual = op @ state
    assert np.allclose(actual, expected)
