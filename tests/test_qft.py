from qint.quantum_fourier import qft
from qint import H
from qint.main import Machine
import numpy as np


"""
(
  (GATE #2A((0.7071067811865475d0 0.7071067811865475d0) (0.7071067811865475d0 -0.7071067811865475d0)) 0)
  (GATE #2A((1 0 0 0) (0 0 1 0) (0 1 0 0) (0 0 0 1)) 0 2)
)
"""


def test_qft_3_qubits():
    inst = qft([0, 1, 2])

    assert len(inst) == 8
    assert np.allclose(inst[0].gate, H)
    assert np.allclose(inst[0].qubits, [2])
    assert np.allclose(inst[1].gate, np.array([[1, 0, 0, 0],
                                               [0, 1, 0, 0],
                                               [0, 0, 1, 0],
                                               [0, 0, 0, 1j]]))
    assert np.allclose(inst[1].qubits, [1, 2])
    assert np.allclose(inst[2].gate, H)
    assert np.allclose(inst[2].qubits, [1])
    assert np.allclose(inst[3].gate, np.array([[1, 0, 0, 0],
                                               [0, 0, 1, 0],
                                               [0, 1, 0, 0],
                                               [0, 0, 0, 1]]))
    assert np.allclose(inst[3].qubits, [1, 2])
    assert np.allclose(inst[4].gate, np.array([[1, 0, 0, 0],
                                               [0, 1, 0, 0],
                                               [0, 0, 1, 0],
                                               [0, 0, 0, np.exp(1j * np.pi / 4)]]))
    assert np.allclose(inst[4].qubits, [0, 1])
    assert np.allclose(inst[5].gate, np.array([[1, 0, 0, 0],
                                               [0, 1, 0, 0],
                                               [0, 0, 1, 0],
                                               [0, 0, 0, 1j]]))
    assert np.allclose(inst[5].qubits, [0, 2])
    assert np.allclose(inst[6].gate, H)
    assert np.allclose(inst[6].qubits, [0])
    assert np.allclose(inst[7].gate, np.array([[1, 0, 0, 0],
                                               [0, 0, 1, 0],
                                               [0, 1, 0, 0],
                                               [0, 0, 0, 1]]))
    assert np.allclose(inst[7].qubits, [0, 2])


def test_run_qft():
    inst = qft([0, 1, 2])
    m = Machine.init(3)
    classical_fft = np.fft.fft(m.quantum_state, norm="ortho")
    m.run(inst)
    assert np.allclose(m.quantum_state, classical_fft)
