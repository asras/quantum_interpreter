import numpy as np
from qint.main import Instruction, Machine
from qint import H, CNOT
from qint.permutation_utils import invert_perm
from qint.utils import lift, bit_string_idx, permutation_to_operator


SRC = [
    "GATE; [[0.707, 0.707], [0.707, -0.707]]; 2",
    "GATE; [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 1], [0, 0, 1, 0]]; 2 5",
    "MEASURE"
]

SRC2 = [
    "GATE; [[0.707, 0.707], [0.707, -0.707]]; 2",
    "GATE; [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 1], [0, 0, 1, 0]]; 2 5",
    "GATE; [[1, 0], [0, 0]]; 2",  # Projection gate for testing
    "MEASURE"
]


def test_instruction():
    instructions = [Instruction.from_string(s) for s in SRC]

    assert instructions[0].type == 'GATE'
    assert instructions[0].gate.shape == (2, 2)
    assert instructions[0].qubits == [2]

    assert instructions[1].type == 'GATE'
    assert instructions[1].gate.shape == (4, 4)
    assert instructions[1].qubits == [2, 5]

    assert instructions[2].type == 'MEASURE'
    assert instructions[2].qubits is None
    assert instructions[2].gate is None


def test_run():
    instructions = [Instruction.from_string(s) for s in SRC]
    machine = Machine.init(6)
    machine.run(instructions)


def test_run_projection():
    instructions = [Instruction.from_string(s) for s in SRC2]
    machine = Machine.init(6)
    machine.run(instructions)
    assert machine.get_measurement_bit(2) == 0


def test_bell():
    """Create a Bell state and measure it"""
    instructions = [
        Instruction("GATE", H, [0]),
        Instruction("GATE", CNOT, [1, 0]),
    ]
    machine = Machine.init(2)
    machine.run(instructions)
    assert np.allclose(machine.quantum_state, [1 / np.sqrt(2), 0, 0, 1 / np.sqrt(2)])
    machine.run([Instruction("MEASURE", None, None)])
    assert machine.get_measurement_bit(0) == machine.get_measurement_bit(1)


def test_cnot_01_10():
    def make_cnot(q1, q2):
        U = lift(CNOT, 0, 2)
        qubits = [q1, q2]
        perm = [i for i in qubits] \
            + [i for i in range(2) if i not in qubits]
        pi = permutation_to_operator(perm, 2)
        piinv = permutation_to_operator(invert_perm(perm), 2)
        assert np.allclose(pi @ piinv, np.eye(2**2))
        op = piinv @ U @ pi
        return op, pi, piinv

    op1, tf1, ft1 = make_cnot(1, 0)
    assert np.allclose(bit_string_idx(op1, "00", "00"), 1)
    assert np.allclose(bit_string_idx(op1, "11", "01"), 1)
    assert np.allclose(bit_string_idx(op1, "10", "10"), 1)
    assert np.allclose(bit_string_idx(op1, "01", "11"), 1)
    op2, tf2, ft2 = make_cnot(0, 1)
    assert np.allclose(bit_string_idx(op2, "00", "00"), 1)
    assert np.allclose(bit_string_idx(op2, "01", "01"), 1)
    assert np.allclose(bit_string_idx(op2, "11", "10"), 1)
    assert np.allclose(bit_string_idx(op2, "10", "11"), 1)
    assert not np.allclose(tf1, tf2)
    assert not np.allclose(ft1, ft2)
    assert not np.allclose(op1, op2)


def test_cnot_12_21():
    def make_cnot(q1, q2):
        U = lift(CNOT, 0, 3)
        qubits = [q1, q2]
        perm = [i for i in qubits] \
            + [i for i in range(3) if i not in qubits]

        pi = permutation_to_operator(perm, 3)
        piinv = permutation_to_operator(invert_perm(perm), 3)
        assert np.allclose(pi @ piinv, np.eye(2**3))
        op = piinv @ U @ pi
        return op, pi, piinv

    op1, tf1, ft1 = make_cnot(1, 2)
    assert np.allclose(bit_string_idx(op1, "000", "000"), 1)
    assert np.allclose(bit_string_idx(op1, "010", "010"), 1)
    assert np.allclose(bit_string_idx(op1, "111", "101"), 1)
    assert np.allclose(bit_string_idx(op1, "110", "100"), 1)
    assert np.allclose(bit_string_idx(op1, "100", "110"), 1)
    op2, tf2, ft2 = make_cnot(2, 1)
    assert np.allclose(bit_string_idx(op2, "000", "000"), 1)
    assert np.allclose(bit_string_idx(op2, "110", "010"), 1)
    assert np.allclose(bit_string_idx(op2, "101", "101"), 1)
    assert np.allclose(bit_string_idx(op2, "100", "100"), 1)
    assert np.allclose(bit_string_idx(op2, "010", "110"), 1)
    assert not np.allclose(tf1, tf2)
    assert not np.allclose(ft1, ft2)
    assert not np.allclose(op1, op2)


def test_cnot_32_23():
    def make_cnot(q1, q2):
        U = lift(CNOT, 0, 4)
        qubits = [q1, q2]
        perm = [i for i in qubits] \
            + [i for i in range(4) if i not in qubits]
        pi = permutation_to_operator(perm, 4)
        piinv = permutation_to_operator(invert_perm(perm), 4)
        assert np.allclose(pi @ piinv, np.eye(2**4))
        op = piinv @ U @ pi
        return op, pi, piinv

    op1, tf1, ft1 = make_cnot(2, 3)
    assert np.allclose(bit_string_idx(op1, "0000", "0000"), 1)
    assert np.allclose(bit_string_idx(op1, "0100", "0100"), 1)
    assert np.allclose(bit_string_idx(op1, "1110", "1010"), 1)
    assert np.allclose(bit_string_idx(op1, "1100", "1000"), 1)
    assert np.allclose(bit_string_idx(op1, "1000", "1100"), 1)
    op2, tf2, ft2 = make_cnot(3, 2)
    assert np.allclose(bit_string_idx(op2, "0000", "0000"), 1)
    assert np.allclose(bit_string_idx(op2, "1100", "0100"), 1)
    assert np.allclose(bit_string_idx(op2, "1010", "1010"), 1)
    assert np.allclose(bit_string_idx(op2, "1000", "1000"), 1)
    assert np.allclose(bit_string_idx(op2, "0100", "1100"), 1)
    assert not np.allclose(tf1, tf2)
    assert not np.allclose(ft1, ft2)
    assert not np.allclose(op1, op2)


def test_ghz():
    """Create a Greenberger-Horne-Zeilinger state and measure it"""
    n_bits = 4
    instructions = [Instruction("GATE", H, [0])] \
        + [Instruction("GATE", CNOT, [i + 1, i]) for i in range(3)]
    for inst in instructions:
        print(inst)
    machine = Machine.init(n_bits)
    machine.run(instructions)
    assert np.allclose(machine.quantum_state, [1 / np.sqrt(2), 0, 0, 0,
                                               0, 0, 0, 0,
                                               0, 0, 0, 0,
                                               0, 0, 0, 1 / np.sqrt(2)])
    machine.run([Instruction("MEASURE", None, None)])
    assert machine.get_measurement_bit(0) == machine.get_measurement_bit(1)
    assert machine.get_measurement_bit(0) == machine.get_measurement_bit(2)
    assert machine.get_measurement_bit(0) == machine.get_measurement_bit(3)
