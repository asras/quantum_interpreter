from qint.permutation_utils import permutation_to_transpositions, \
    transposition_to_adjacent_transpositions, \
    transpositions_to_adjacent_transpositions, invert_perm
from qint.utils import transpositions_to_operator
import numpy as np


def test_single_transposition():
    perm = "102345"
    transpositions = permutation_to_transpositions(perm)
    assert transpositions == [(0, 1)]


def test_non_local_transposition():
    perm = "512340"
    transpositions = permutation_to_transpositions(perm)
    assert transpositions == [(0, 5)]


def test_multiple_transpositions():
    perm = "103254"
    transpositions = permutation_to_transpositions(perm)
    assert transpositions == [(0, 1), (2, 3), (4, 5)]


def test_transpos_to_adjacent_transpos():
    adjacents = transposition_to_adjacent_transpositions((0, 2))
    assert adjacents == [0, 1, 0]

    adjacents = transposition_to_adjacent_transpositions((0, 3))
    assert adjacents == [0, 1, 2, 1, 0]


def test_adjacent_transpos_to_single():
    adjacents = transposition_to_adjacent_transpositions((0, 1))
    assert adjacents == [0]


def test_adjacent_transpositions():
    adjacents = transpositions_to_adjacent_transpositions([(0, 2), (1, 3)])
    assert adjacents == [0, 1, 0, 1, 2, 1]


def test_adjacent_transpositions_elimination():
    adjacents = transpositions_to_adjacent_transpositions([(0, 2), (0, 1)])
    assert adjacents == [0, 1]


def test_article_example():
    perm = "34021"
    trans = permutation_to_transpositions(perm)
    assert trans == [(0, 3), (1, 4), (2, 3)]


def apply_swap(swap, ls):
    i, j = swap
    ls[i], ls[j] = ls[j], ls[i]


def test_permutation_to_transpositions():
    perm = "0123"
    ls = ["a", "b", "c", "d"]
    trans = permutation_to_transpositions(perm)
    for t in trans:
        apply_swap(t, ls)

    assert ls == ["a", "b", "c", "d"]

    perm = "3210"
    ls = ["a", "b", "c", "d"]
    trans = permutation_to_transpositions(perm)
    for t in trans:
        apply_swap(t, ls)

    assert ls == ["d", "c", "b", "a"]

    perm = "3120"
    ls = ["a", "b", "c", "d"]
    trans = permutation_to_transpositions(perm)
    for t in trans:
        apply_swap(t, ls)

    assert ls == ["d", "b", "c", "a"]

    perm = "1320"
    ls = ["a", "b", "c", "d"]
    trans = permutation_to_transpositions(perm)
    print(trans)
    for t in trans:
        apply_swap(t, ls)

    assert ls == ["b", "d", "c", "a"]

    perm = "1302"
    ls = ["a", "b", "c", "d"]
    trans = permutation_to_transpositions(perm)
    print(trans)
    for t in trans:
        apply_swap(t, ls)

    assert ls == ["b", "d", "a", "c"]


def apply_adj(idx, ls):
    ls[idx], ls[idx + 1] = ls[idx + 1], ls[idx]


def test_permutation_to_adj_transpositions():
    perm = "0123"
    ls = ["a", "b", "c", "d"]
    trans = transpositions_to_adjacent_transpositions(permutation_to_transpositions(perm))
    for t in trans:
        apply_adj(t, ls)

    assert ls == ["a", "b", "c", "d"]

    perm = "3210"
    ls = ["a", "b", "c", "d"]
    trans = transpositions_to_adjacent_transpositions(permutation_to_transpositions(perm))
    for t in trans:
        apply_adj(t, ls)

    assert ls == ["d", "c", "b", "a"]

    perm = "3120"
    ls = ["a", "b", "c", "d"]
    trans = transpositions_to_adjacent_transpositions(permutation_to_transpositions(perm))
    for t in trans:
        apply_adj(t, ls)

    assert ls == ["d", "b", "c", "a"]

    perm = "1320"
    ls = ["a", "b", "c", "d"]
    trans = transpositions_to_adjacent_transpositions(permutation_to_transpositions(perm))
    print(trans)
    for t in trans:
        apply_adj(t, ls)

    assert ls == ["b", "d", "c", "a"]

    perm = "1302"
    ls = ["a", "b", "c", "d"]
    trans = transpositions_to_adjacent_transpositions(permutation_to_transpositions(perm))
    print(trans)
    for t in trans:
        apply_adj(t, ls)

    assert ls == ["b", "d", "a", "c"]


def test_from_space_neq():
    qubits = [2, 3]
    from_space = [i for i in reversed(qubits)] \
        + [i for i in range(4) if i not in qubits]

    trans = transpositions_to_adjacent_transpositions(
        permutation_to_transpositions(from_space))
    to_from = transpositions_to_operator(trans, 4)
    from_to = transpositions_to_operator(list(reversed(trans)), 4)

    qubits2 = [3, 2]
    from_space2 = [i for i in reversed(qubits2)] \
        + [i for i in range(4) if i not in qubits2]
    assert from_space != from_space2

    trans2 = transpositions_to_adjacent_transpositions(
        permutation_to_transpositions(from_space2))
    assert trans != trans2
    to_from2 = transpositions_to_operator(trans2, 4)
    from_to2 = transpositions_to_operator(list(reversed(trans2)), 4)

    assert not np.allclose(to_from, to_from2)
    assert not np.allclose(from_to, from_to2)


def test_invert_perm_twice():
    perm = "3021"
    assert perm == invert_perm(invert_perm(perm))
    perm = "3201"
    assert perm == invert_perm(invert_perm(perm))
    perm = "1032"
    assert perm == invert_perm(invert_perm(perm))


def test_invert_perm_specific():
    perm = "3021"
    assert invert_perm(perm) == "1320"
    perm = "3201"
    assert invert_perm(perm) == "2310"
    perm = "1032"
    assert invert_perm(perm) == "1032"
